package com.martin.demo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Note {
    @JsonIgnore
    @Id
    @GeneratedValue
    private Long noteId;

    private String content;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Note(){

    }

    public Note(String content){
        this.content = content;
    }

    public Note(String content, User user){
        this.content = content;
        this.user = user;
    }

    public Long getNoteId(){
        return noteId;
    }

    public void setNoteId(Long id){
        this.noteId = noteId;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }

}
