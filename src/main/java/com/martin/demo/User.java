package com.martin.demo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="users")
public class User {

    @JsonIgnore
    @Id
    @GeneratedValue
    private Long userId;

    private String firstName;
    private String lastName;

    @OneToMany(targetEntity = Note.class, mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Note> notes = new HashSet<>();

    protected User() {
        super();
    }

    public User(Long personId, String firstName, String lastName) {
        super();
        this.userId = personId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @JsonIgnore
    public Long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public Set<Note> getNotes(){
        return notes;
    }

    public void setNotes(Set<Note> notes){
        this.notes = notes;
    }


    @Override
    public String toString(){
        return String.format("User[id=%d, firstName='%s', lastName='%s']", userId, firstName, lastName);
    }


}
