package com.martin.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("users")
public class UserResource {

    @Autowired
    private UserRepository userRepository;
    private NoteRepository noteRepository;

    UserResource(NoteRepository noteRepository, UserRepository userRepository){
        this.noteRepository = noteRepository;
        this.userRepository = userRepository;
    }

    @RequestMapping(value = {"", "/", "/list"}, method = RequestMethod.GET)
    public Collection<User> listUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<User> listUser(@PathVariable String id)
    {
        Long userId = Long.valueOf(id);

        this.validatePerson(userId);
        return this.userRepository.findByUserId(userId);
    }

    @PostMapping
    ResponseEntity<?> add(@RequestBody User input){
        User savedUser = userRepository.save(input);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedUser.getUserId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    ResponseEntity<?> edit(@PathVariable String id,@RequestBody User input){
        Long userId = Long.valueOf(id);
        return userRepository.findByUserId(userId)
                .map(user -> ResponseEntity.created(
                        ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                        .buildAndExpand(userRepository.save(input).getUserId()).toUri())
                        .build())
                .orElse(ResponseEntity.notFound().build());
    }

    private void validatePerson(Long userId) {
        this.userRepository
                .findByUserId(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    @DeleteMapping
    ResponseEntity<?> delete(@PathVariable String id){
        Long userId = Long.valueOf(id);
        this.validatePerson(userId);

        User userToDelete = userRepository.getOne(userId);

        userRepository.delete(userToDelete);
        return ResponseEntity.noContent().build();
    }

    //TODO PatchMapping


}
