package com.martin.demo;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(Long id){
        super("Could not find used with id: "+id.toString());
    }
}
