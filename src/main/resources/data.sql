insert into groups
values(111,"admin")

insert into groups
values (112,"users")

insert into groups (id, name, parent_id)
values (113, "testUsers", 112)

insert into users
values(11,'user1','test1');

insert into users
values(12,'user2','test2');

insert into note (note_id, content, user_id)
values(1, 'note1', 11);

insert into note (note_id, content, user_id)
values(2, 'note2', 11);

insert into note (note_id, content, user_id)
values(3, 'note3', 11);

insert into note (note_id, content, user_id)
values(4, 'note4', 12);

insert into note (note_id, content, user_id)
values(5, 'note5', 12);


